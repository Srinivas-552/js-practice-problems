/*

   Q. Read the result.json and gather data of all the people who failed in both their exams.
        Less than 35 score is failed.
   Q. Gather data of students who has failed in only computer science.

   Q. Calculate average for each student and save it to a new file

   Q. Sort data with student name and sort the subjects array on alphabetical order as well.

   Q. Generate result of user in this form and write it to a new file.
        ComputerScience - computersciencemarks , Mathematics - mathematicsMarks : studentName

        Display result in each separate line.

*/
const fs = require("fs");

// const jsonData = require('./result.json');

function readFileData(filePath) {
  return new Promise((res, rej) => {
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        rej(err);
      } else {
        res(JSON.parse(data));
      }
    });
  });
}


const failedStudents = (data) => {
  return Object.keys(data).reduce((result, curr) => {
    const [studentData] = data[curr];
    if (studentData.Mathematics < 35 && studentData["Computer Science"] < 35) {
      result[curr] = studentData;
    }
    return result;
  }, {});
};


const failedStudentsInCs = (data) => {
  return Object.keys(data).reduce((result, curr) => {
    const [studentData] = data[curr];
    if (studentData["Computer Science"] < 35) {
      result[curr] = studentData;
    }
    return result;
  }, {});
};


const studentAverage = (data) => {
  const newdata = Object.keys(data).reduce((result, curr) => {
    const [studentData] = data[curr];
    const avg = (studentData.Mathematics + studentData["Computer Science"]) / 2;
    result[curr] = { avg };
    return result;
  }, {});
  return new Promise((res, rej) => {
    fs.writeFile("Average.json", JSON.stringify(newdata), "utf8", (err) => {
      if (err) {
        rej(err);
      } else {
        res("Average.json File created");
      }
    });
  });
};


const sortStudent = (data)=>{
     const sortData = Object.keys(data).sort(((a, b) => {
          if (a < b){
               return -1;
          } else if ( a > b){
               return 1;
          } else {
               return 0;
          }
     }))
    //  .forEach(student => {
    //       let [newdata] = data[student]
    //       const res = Object.keys(newdata).sort((a,b) => {
    //            return a.localeCompare(b)
    //       })
          
    //  })
  return sortData;
}


const GenerateResult=(data)=>{
     const newdata = Object.keys(data).reduce((result, eachItem)=>{
          const [each] = data[eachItem]
          result += `ComputerScience - ${each["Computer Science"]} , Mathematics - ${each.Mathematics} : ${eachItem}; `
          return result;
     }, '');
     return new Promise((res, rej)=> {
          fs.writeFile("dataFormat.json", JSON.stringify(newdata), (err) => {
               if(err){
                    rej(err)
               } else {
                    res("dataFormat.json new file created")
               }
          })
     })
}

(async function () {
  const data = await readFileData("./result.json");
  const failedData = failedStudents(data);
  const csFailedData = failedStudentsInCs(data);
  const avgRes = await studentAverage(data)
  const resultForm = await GenerateResult(data)
  const sortResult = sortStudent(data)
  console.log(sortResult)
})();
